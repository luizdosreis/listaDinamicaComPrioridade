/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: luizhenrique
 *
 * Created on 14 de Agosto de 2016, 21:08
 */

#include <iostream>
#include "fila.h"

using namespace std;


void opcoes(){
    cout <<"Escolha a opcao desejada: " << endl
    <<"1 - cria" << endl
    <<"2 - insere" << endl
    <<"3 - retira" << endl
    <<"4 - mostra" << endl
    <<"5 - verifica se esta vazia" << endl
    <<"6 - numero de elementos" << endl
    <<"7 - procura elemento pela posicao" << endl
    <<"8 - procura posicao do elemento " << endl
    <<"9 - primeiro elemento da fila" << endl
    <<"10 - ultimo elemento da fila" << endl
    <<"11 - sair"<< endl;
}

void insereNaFila(FilaDinamicaComPrioridade &fila){
    char elemento, prioridade;
    cout <<"Insira o elemento desejado" << endl;
    cin >> elemento;
    do{
        cout <<"Insira a prioridade alta(A), media(M) ou baixa(B)" <<endl;
        cin >> prioridade;
        prioridade = toupper(prioridade);
        if(prioridade!='B' and prioridade!='M' and prioridade!='A') 
            cout <<"Prioridade invalida, insira novamente"<< endl;
    }while(prioridade!='B' and prioridade!='M' and prioridade!='A');
    insere(fila,elemento,prioridade);
}

void retiraDaFila(FilaDinamicaComPrioridade &fila){
    cout << "Elemento Retirado "<< primeiroElemento(fila) << endl;
    retira(fila);
}

void mostraFila(FilaDinamicaComPrioridade fila){
    mostra(fila);
    cout << endl;
}

void filaVazia(FilaDinamicaComPrioridade fila){
    if(ehVazia(fila)) cout <<"Esta vazia" << endl;
    else cout <<"Nao esta vazia" << endl;
}

void numeroDeElementosDaFila(FilaDinamicaComPrioridade fila){
    cout << "O numero de elementos é: " << numeroDeElementos(fila) << endl;
}

void procuraElementoPelaPosicao(FilaDinamicaComPrioridade fila){
    int posicao;
    do{
        cout <<"Informe a posicao desejada" << endl;
        cin >> posicao;
        if(posicao < 1 and posicao > fila.numElementos) cout << "Posicao invalida, Digite novamente" << endl;
    }while(posicao < 1 and posicao > fila.numElementos);
    cout << "Elemento: " << umElemento(fila,posicao) << endl;
}

void procuraPosicaoDoElemento(FilaDinamicaComPrioridade fila){
    char elemento;
    do{
        cout << "Insira o elemento que deseja pesquisa" << endl;
        cin >> elemento;
        if(!existeElemento(fila,elemento))
            cout << "Elemento nao existe na fila, insira novamente" << endl;
    }while(!existeElemento(fila,elemento));
    cout << "A posicao do elemente e " << posicao(fila,elemento) << endl;  
}

void primeiroElementoDaFila(FilaDinamicaComPrioridade fila){
    cout << "Primeiro elemento: " << primeiroElemento(fila) << endl;
}

void ultimoElementoDaFila(FilaDinamicaComPrioridade fila){
    cout << "Ultimo elemento: " << ultimoElemento(fila) << endl;
}

void menu(int opcao, FilaDinamicaComPrioridade &fila){
    switch (opcao){
        case 1:
            fila = cria();
            break;
        case 2:
            insereNaFila(fila);
            break;
        case 3:
            retiraDaFila(fila);
            break;
        case 4:
            mostraFila(fila);
            break;
        case 5:
            filaVazia(fila);
            break;
        case 6:
            numeroDeElementosDaFila(fila);
            break;
        case 7:
            procuraElementoPelaPosicao(fila);
            break;
        case 8:
            procuraPosicaoDoElemento(fila);
            break;
        case 9:
            primeiroElementoDaFila(fila);
            break;
        case 10:
            ultimoElementoDaFila(fila);
            break;    
    }
}

/*
 * 
 */
int main(int argc, char** argv) {
    FilaDinamicaComPrioridade fila;
    int opcao;
    do{
        opcoes();
        cin >> opcao;
        menu(opcao,fila);
        if(opcao < 1 and opcao > 11)
            cout << "Opcao invalida, digite novamente" << endl;
    }while(opcao!=11);
    destroi(fila);
    return 0;
}


